$(document).ready(function() {

    $('#btn-read-all').click(function() {
        let request = 'http://www.localhost:8080/?command=readAll';
        $.getJSON(request)
            .done(function (data) {
                $('#panel-body').empty();
                for (let i in data) {
                    printOrder(data[i], i);
                }
            })
            .fail(function (jqXHR, textStatus, err) {
                console.log( "Request Failed: " + textStatus + ", " + err);
            });
    });

    $('#btn-read-by-id').click(function() {
        let order_id = $('#input-read-by-id').val();
        if (order_id) {
            readById(order_id, function (data) {
                $('#panel-body').empty();
                if (data.message != null && data.status != null) {
                    alert(data.message);
                } else {
                    printOrder(data, 0, false);
                }
            });
        }
    });

    function readById(order_id, done) {
        let request = 'http://www.localhost:8080/?command=readById&order_id=' + order_id;
        $.getJSON(request)
            .done(function (data) {
                done(data);
            })
            .fail(function (jqXHR, textStatus, err) {
                console.log( "Request Failed: " + textStatus + ", " + err);
            });
    }

    function readByCartId(cart_id, done) {
        let request = 'http://www.localhost:8080/?command=readByCartId&cart_id=' + cart_id;
        $.getJSON(request)
            .done(function (data) {
                done(data);
            })
            .fail(function (jqXHR, textStatus, err) {
                console.log( "Request Failed: " + textStatus + ", " + err);
            });
    }

    $('#btn-add-to-cart').click(function() {
        let cart_id = $('#input-add-to-cart').val();
        if (cart_id) {
            let request = 'http://www.localhost:8080/?command=addToCart&cart_id=' + cart_id;
            $.getJSON(request)
                .done(function (data) {
                    alert('New product added with id: ' + data);
                    readByCartId(cart_id, function (data) {
                        $('#panel-body').empty();
                        if (data.message != null && data.status != null) {
                            alert(data.message);
                        } else {
                            printOrder(data, 0, false);
                        }
                    });
                })
                .fail(function (jqXHR, textStatus, err) {
                    console.log( "Request Failed: " + textStatus + ", " + err);
                });
        }
    });

    $('#btn-del-by-id').click(function() {
        let order_id = $('#input-del-order-id').val();
        let product_id = $('#input-del-prod-id').val();
        if (order_id && product_id) {
            let request = 'http://www.localhost:8080/?command=delById&order_id=' + order_id + '&product_id=' + product_id;
            $.getJSON(request)
                .done(function (data) {
                    alert(data.message);
                    if (data.status == true) {
                        readById(order_id, function (data) {
                            $('#panel-body').empty();
                            printOrder(data, 0, false);
                        });
                    }
                })
                .fail(function (jqXHR, textStatus, err) {
                    console.log( "Request Failed: " + textStatus + ", " + err);
                });
        }
    });

    function printOrder(order, i, collapsed = true) {
        $('#panel-body').append('<div id="order-panel-' + i + '" class="panel panel-default" style="max-width: 70%; margin: auto auto 10px;"></div>');
        $('#order-panel-' + i).append('<div id="order-panel-heading-' + i + '" class="panel-heading"></div>');
        $('#order-panel-heading-' + i).append('<div id="order-panel-title-' + i + '" class="panel-title"></div>');
        $('#order-panel-title-' + i).append('<a id="order-title-' + i + '" data-toggle="collapse" href="#collapse-' + i + '"></a>');

        let title = $('#order-title-' + i);
        title.append('Order ID: ' + order.id)
            .append(' Customer: ' + order.credentials.name + ' ' + order.credentials.surname)
            .append(' (Cart ID: ' + order.cart.id + ')');

        $('#order-panel-' + i).append('<div id="collapse-' + i + '" class="panel-collapse"></div>');
        $('#collapse-' + i).addClass(collapsed ? 'collapse' : 'in');

        $('#collapse-' + i).append('<div id="order-panel-body-' + i + '" class="panel-body" style="padding:0"></div>');
        $('#order-panel-body-' + i).append('<table id="order-table-' + i + '" class="table" style="margin-bottom:0"></table>');

        let cart = order.cart;
        if (cart.products.length > 0) {
            $('#order-table-' + i).append('<tr id="table-' + i + '-header"></tr>');
            $('#table-' + i + '-header')
                .append('<th class="text-left">Product</th>')
                .append('<th class="text-left">Producer</th>')
                .append('<th class="text-left">Provider</th>')
                .append('<th class="text-right">Price</th>')
                .append('<th class="text-center">ID</th>');

            for (let j in cart.products) {
                let product = cart.products[j];
                $('#order-table-' + i).append('<tr id="table-' + i + '-row-' + j + '"></tr>');
                $('#table-' + i + '-row-' + j)
                    .append('<td class="text-left">' + product.type.substring(product.type.lastIndexOf('.')+1) + '</td>')
                    .append('<td class="text-left">' + product.producer + '</td>')
                    .append('<td class="text-left">' + product.provider + '</td>')
                    .append('<td class="text-right">' + product.price.toFixed(2) + '</td>')
                    .append('<td class="text-right" style="padding-top: 12px; font-size: 10px">' + product.id + '</td>');
            }
        }
    }
});
