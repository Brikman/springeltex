package com.eltex.controller;

import com.eltex.dao.CartRepository;
import com.eltex.dao.ProductRepository;
import com.eltex.model.*;
import com.eltex.dao.OrderRepository;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin("http://localhost:8080")
@RestController
public class OrderController {
    private static final Logger LOGGER = LogManager.getLogger(OrderController.class);

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ProductRepository productRepository;

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> process(
            @RequestParam("command") String command,
            @RequestParam(value = "order_id",   required = false) Long orderId,
            @RequestParam(value = "cart_id",    required = false) Long cartId,
            @RequestParam(value = "product_id", required = false) String productId
    ) {
        switch (command) {
            case "readAll" :
                return readAll();
            case "readById" :
                return readById(orderId);
            case "readByCartId" :
                return readByCartId(cartId);
            case "addToCart" :
                return addToCart(cartId);
            case "delById" :
                return deleteById(orderId, productId);
        }
        return ResponseEntity.ok(new ResponseMessage("incorrect command: " + command, false));
    }

    private ResponseEntity<Object> readAll() {
        List<Order> orders = orderRepository.findAll();
        return !orders.isEmpty() ?
                ResponseEntity.ok(orders) :
                ResponseEntity.ok(new ResponseMessage("no orders found", false));
    }

    private ResponseEntity<Object> readById(Long orderId) {
        Order order = orderRepository.findOne(orderId);
        return order != null ?
                ResponseEntity.ok(order) :
                ResponseEntity.ok(new ResponseMessage("cannot find order with given id: " + orderId, false));
    }

    private ResponseEntity<Object> readByCartId(Long cartId) {
        List<Order> orders = orderRepository.findAll();
        for (Order order : orders) {
            if (order.getCart().getId() == cartId) {
                return ResponseEntity.ok(order);
            }
        }
        return ResponseEntity.ok(new ResponseMessage("cannot find order by given cart id: " + cartId, false));
    }

    private ResponseEntity<Object> addToCart(Long cartId) {
        ShoppingCart cart = cartRepository.findOne(cartId);
        if (cart != null) {
            Product product = EntityGenerator.generateProduct();
            product = productRepository.saveAndFlush(product);
            cart.add(product);
            cartRepository.saveAndFlush(cart);
            return ResponseEntity.ok(product.getId());
        }
        return ResponseEntity.ok(new ResponseMessage("cannot find cart with given id: " + cartId, false));
    }

    private ResponseEntity<Object> deleteById(Long orderId, String productId) {
        Order order = orderRepository.findOne(orderId);
        if (order != null) {
            ShoppingCart cart = order.getCart();
            boolean deleted = cart.delete(UUID.fromString(productId));
            if (deleted) {
                orderRepository.saveAndFlush(order);
                return ResponseEntity.ok(new ResponseMessage("product deleted", true));
            } else {
                return ResponseEntity.ok(new ResponseMessage("cannot find product with given id: " + productId, false));
            }
        } else {
            return ResponseEntity.ok(new ResponseMessage("cannot find order with given id: " + orderId, false));
        }
    }
}