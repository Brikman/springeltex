package com.eltex.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

@Entity
@DiscriminatorValue("tea")
public class Tea extends Product {

    @Enumerated(EnumType.STRING)
    private Pack pack;

    @Override
    public void create() {
        super.create();
        Random rnd = new Random();
        Pack[] packs = Pack.values();
        pack = packs[rnd.nextInt(packs.length - 1)];
    }

    @Override
    public void read() {
        System.out.println(this);
    }

    @Override
    public void update() {
        super.update();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("pack >> ");
            pack = Pack.valueOf(reader.readLine().toUpperCase());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Pack getPack() {
        return pack;
    }

    @Override
    public String toString() {
        return super.toString() + "\npack : " + pack;
    }

    @Override
    public void delete() {
        super.delete();
        pack = null;
    }

    public enum Pack {
        BOX,
        BAG,
        PACKET
    }
}
