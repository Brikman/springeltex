package com.eltex.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "credentials")
public class Credentials implements Serializable {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "BINARY(16)")
    private UUID id;

    @NotBlank
    private String name;

    @NotBlank
    private String surname;

    private String patronymic;

    @NotBlank
    private String email;

    public Credentials() {
    }

    @JsonCreator
    public Credentials(
            @JsonProperty("name") String name,
            @JsonProperty("surname") String surname,
            @JsonProperty("patronymic") String patronymic,
            @JsonProperty("email") String email) {
        this.name       = name;
        this.surname    = surname;
        this.patronymic = patronymic;
        this.email      = email;
    }

    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
    public String getPatronymic() {
        return patronymic;
    }
    public String getEmail() {
        return email;
    }
    public UUID   getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append(id).append(" : ")
                .append(surname).append(" ")
                .append(name).append(" ")
                .append(patronymic).append(" : ")
                .append(email)
                .toString();
    }
}
