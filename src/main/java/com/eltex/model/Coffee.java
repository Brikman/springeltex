package com.eltex.model;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

@Entity
@DiscriminatorValue("coffee")
public class Coffee extends Product {

    @Enumerated(EnumType.STRING)
    private Bean bean;

    @Override
    public void create() {
        super.create();
        Random rnd = new Random();
        Bean[] packs = Bean.values();
        bean = packs[rnd.nextInt(packs.length - 1)];
    }

    @Override
    public void read() {
        System.out.println(this);
    }

    @Override
    public void update() {
        super.update();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("bean >> ");
            bean = Bean.valueOf(reader.readLine().toUpperCase());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete() {
        super.delete();
        bean = null;
    }

    @JsonGetter("bean")
    public Bean getBean() {
        return bean;
    }

    @Override
    public String toString() {
        return super.toString() + "\nbean : " + bean;
    }

    public enum Bean {
        ARABICA,
        ROBUSTA
    }
}
