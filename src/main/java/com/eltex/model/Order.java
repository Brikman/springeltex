package com.eltex.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="cart_id")
    private ShoppingCart cart;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "creden_id")
    private Credentials credentials;

    @Enumerated(EnumType.STRING)
    private Status status;

    public Order() {
    }

    @JsonCreator
    public Order(@JsonProperty("cart") ShoppingCart cart, @JsonProperty("credentials") Credentials credentials) {
        this.cart = cart;
        this.credentials = credentials;
        status = Status.PENDING;
    }

    public void process() {
        status = Status.PROCESSED;
    }

    public ShoppingCart getCart() {
        return cart;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public Status getStatus() {
        return status;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("[ ID:").append(id).append(" ]")
                .append(" (").append(status).append(")\n")
                .append(credentials)
                .append("\n............................\n")
                .append(cart)
                .toString();
    }

    public enum Status {
        PENDING,
        PROCESSED
    }
}
