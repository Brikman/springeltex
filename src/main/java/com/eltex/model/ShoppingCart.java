package com.eltex.model;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;
import java.util.function.Consumer;

@Entity
@Table(name = "shopping_carts")
public class ShoppingCart implements Serializable, Iterable<Product> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = "cart_product",
            joinColumns = { @JoinColumn(name = "cart_id") },
            inverseJoinColumns = { @JoinColumn(name = "product_id") }
    )
    private List<Product> products = new ArrayList<>();

    public boolean add(Product product) {
        return products.add(product);
    }

    public boolean delete(Product product) {
        return products.remove(product);
    }

    public boolean delete(UUID productId) {
        for (int i = 0; i < products.size(); i++) {
            if (products.get(i).getId().equals(productId)) {
                products.remove(i);
                return true;
            }
        }
        return false;
    }

    public Product getProduct(UUID id) {
        for (Product product : products) {
            if (product.getId().compareTo(id) == 0)
                return product;
        }
        return null;
    }

    public long getId() {
        return id;
    }
    public List<Product> getProducts() {
        return products;
    }

    public void setId(long id) {
        this.id = id;
    }
    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        String delim = "\n-----------------\n";
        for (Product product : products)
            builder.append(product).append(delim);
        return builder.toString();
    }

    @Override
    public Iterator<Product> iterator() {
        return products.iterator();
    }

    @Override
    public void forEach(Consumer<? super Product> action) {
        products.forEach(action);
    }

    @Override
    public Spliterator<Product> spliterator() {
        return products.spliterator();
    }
}
