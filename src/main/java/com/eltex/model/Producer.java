package com.eltex.model;

public enum Producer {
    BRAZIL,
    COLUMBIA,
    PERU,
    VIETNAM
}