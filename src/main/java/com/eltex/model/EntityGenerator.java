package com.eltex.model;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class EntityGenerator {
    public static Order generateOrder(List<Credentials> users) {
        return new Order(generateCart(), generateCredentials(users));
    }

    public static Credentials generateCredentials(List<Credentials> users) {
        ThreadLocalRandom rnd = ThreadLocalRandom.current();
        Credentials credentials;
        int i = rnd.nextInt(users.size() + 10);
        if (i < users.size()) {
            credentials = users.get(i);
        } else {
            credentials = new Credentials(
                    RandomStringUtils.random(5 + rnd.nextInt(10), true, false).toLowerCase(),
                    RandomStringUtils.random(5 + rnd.nextInt(10), true, false).toLowerCase(),
                    RandomStringUtils.random(5 + rnd.nextInt(10), true, false).toLowerCase(),
                    RandomStringUtils.random(5 + rnd.nextInt(10), true, false).toLowerCase() + "@gmail.com"
            );
            users.add(credentials);
        }
        return credentials;
    }

    public static ShoppingCart generateCart() {
        ShoppingCart cart = new ShoppingCart();
        ThreadLocalRandom rnd = ThreadLocalRandom.current();
        int number = 1 + rnd.nextInt(4);
        for (int i = 0; i < number; i++) {
            cart.add(generateProduct());
        }
        return cart;
    }

    public static Product generateProduct() {
        Product product = new Random().nextBoolean() ? new Coffee() : new Tea();
        product.create();
        return product;
    }
}
