package com.eltex.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.Random;
import java.util.UUID;

@JsonTypeInfo(
        use      = JsonTypeInfo.Id.CLASS,
        include  = JsonTypeInfo.As.PROPERTY,
        property = "type"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Tea.class),
        @JsonSubTypes.Type(value = Coffee.class),
})

@Entity
@Table(name = "products")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "product_type")
public abstract class Product implements ICrudAction, Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @NotBlank
    private String name;

    @Enumerated(EnumType.STRING)
    private Producer producer;

    @Enumerated(EnumType.STRING)
    private Provider provider;

    private Double price;

    public void create() {
        Random rnd = new Random();
        Producer[] producers = Producer.values();
        Provider[] providers = Provider.values();
        name     = RandomStringUtils.random(5 + rnd.nextInt(10), true, false).toLowerCase();
        producer = producers[rnd.nextInt(producers.length - 1)];
        provider = providers[rnd.nextInt(producers.length - 1)];
        price    = 50 + rnd.nextDouble() * 100;
    }

    public void read() {
        System.out.println(this);
    }

    public void update() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("name >> ");
            name = reader.readLine();
            System.out.print("provider >> ");
            provider = Provider.valueOf(reader.readLine().toUpperCase());
            System.out.print("producer >> ");
            producer = Producer.valueOf(reader.readLine().toUpperCase());
            System.out.print("price >> ");
            price = Double.parseDouble(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void delete() {
        id       = null;
        name     = null;
        producer = null;
        provider = null;
        price    = null;
    }

    public UUID     getId() {
        return id;
    }
    public String   getName() {
        return name;
    }
    public Producer getProducer() {
        return producer;
    }
    public Provider getProvider() {
        return provider;
    }
    public Double   getPrice() {
        return price;
    }

    public void setId(UUID id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setProducer(Producer producer) {
        this.producer = producer;
    }
    public void setProvider(Provider provider) {
        this.provider = provider;
    }
    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append(this.getClass().getSimpleName())
                .append("\nID   : ").append(id)
                .append("\nname : ").append(name)
                .append("\nprod : ").append(producer)
                .append("\nprov : ").append(provider)
                .append("\nprice: ").append(price)
                .toString();
    }
}

